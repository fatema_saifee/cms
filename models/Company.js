var keystone = require('keystone');
var Types = keystone.Field.Types;
/**
 * Company Model
 * ==================
 */

var Company = new keystone.List('Company');

Company.add({
	name: { type: String, initial: true, required: true, default: "" },
	email: { 
		url : { type: Types.Email,initial: true,  many: true },
		source : { type: Types.Select, options: 'Primary, Alternate' }
	},
	created : { type: Types.Datetime, default: Date.now , initial: true},
	// password: { type: Types.Password, initial: true, required: true },
	tags: { type: Types.Relationship,initial: true, ref: 'Tag', many: true },
	phone: { 
		number : { type: Types.Number,initial: true,  many: true },
		source : { type: Types.Select, options: 'Primary, Alternate, Other', initial: true }
	},
	website: {
		url : { type: Types.Url, initial: true, many: true },
		source : { type: Types.Select, options: 'Twitter, Skype, Linkedin, Facebook', initial: true }
	},
	owner : { type: Types.Relationship, initial: true, ref: 'User' },
	address: {
		address: { type: String , default: "" },
		category : { type: Types.Select, options: 'Home, Postal, Office' },
		city : { type: String },
		state : { type: String },
		zip : { type: String },
		country : { type: String },
	}
});

/**
 * Registration
 */
// Company.defaultColumns = 'first_name, ,last_name, email';
Company.register();
