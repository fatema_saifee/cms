var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Address Model
 * ==================
 */

var Address = new keystone.List('Address', {
	autokey: { from: 'address', path: 'key', unique: true },
});

Address.add({
	address: { type: String , required: true, default: "" },
	category : { type: Types.Select, options: 'Home, Postal, Office' },
	city : { type: String },
	state : { type: String },
	zip : { type: String },
	country : { type: String },
});

Address.relationship({ ref: 'Contact', path: 'address' });

Address.register();
