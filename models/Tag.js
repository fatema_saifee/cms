var keystone = require('keystone');

/**
 * Tag Model
 * ==================
 */

var Tag = new keystone.List('Tag', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Tag.add({
	name: { type: String, required: true },
});

/**
 * Relationships
 */
Tag.relationship({ ref: 'Contact', path: 'tags' });
Tag.relationship({ ref: 'Company', path: 'tags' });

Tag.register();
