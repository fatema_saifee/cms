var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Task Model
 * ==========
 */
var Task = new keystone.List('Task');

Task.add({
	task: { type: String, required: true,initial: true, index: true },
	category : { type: Types.Select, options: 'Call, Email, Follow-up, Email, Send', initial:true },
	date : { type: Types.Date, initial: true,},
	time : { type: Types.Datetime, default: Date.now , initial: true},
	owner : { type: Types.Relationship, initial: true, ref: 'User' },
	priority : { type: Types.Select, options: 'High, Normal, Low', initial: true},
	status : { type: Types.Select, options: 'yet To Start, In Progress, Completed ', initial: true},
	related_contacts : { type: Types.Relationship,initial: true, ref: 'Contact' , many: true, initial: true},
	related_details :  { type: Types.Text , initial: true},
	description : { type: Types.Textarea , initial: true},
});


/**
 * Relationships
 */
Task.relationship({ ref: 'User', path: 'users', refPath: 'owner' });
Task.relationship({ ref: 'Contact', path: 'contacts', refPath: 'related_contacts' });


/**
 * Registration
 */
// Task.defaultColumns = 'name, email, isAdmin';
Task.register();
