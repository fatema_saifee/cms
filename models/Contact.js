var keystone = require('keystone');
var Types = keystone.Field.Types;
/**
 * Contact Model
 * ==================
 */

var Contact = new keystone.List('Contact');

Contact.add({
	first_name: { type: String , initial: true, required: true, default: "" },
	last_name: { type: String, initial: true, required: true, default: "" },
	email: { type: Types.Email, initial: true, required: true, many: true },
	company : {type: Types.Relationship,initial: true, ref: 'Company'},
	// password: { type: Types.Password, initial: true, required: true },
	tags: { type: Types.Relationship,initial: true, ref: 'Tag', many: true },
	job_description: { type: Types.Text, initial: true },
	phone: { 
		number : { type: Types.Number,initial: true,  many: true },
		source : { type: String, initial: true }
	},
	website: {
		url : { type: Types.Url, initial: true, many: true },
		source : { type: String, initial: true }
	},
	address: { type: Types.Relationship, initial: true, ref: 'Address' },
	image: { type: Types.CloudinaryImage}

});

/**
 * Registration
 */
// Contact.defaultColumns = 'first_name, ,last_name, email';
Contact.register();
