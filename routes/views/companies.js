var keystone = require('keystone');
var async = require('async');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Init locals
	locals.section = 'blog';
	
	locals.data = {
		companies: [],
	};

	
	// Load the companies
	view.on('init', function (next) {

		var q = keystone.list('Company').paginate({
			page: req.query.page || 1,
			perPage: 10,
			maxPages: 10,
			// filters: {
			// 	state: 'published',
			// },
		})
			// .sort('-publishedDate')
			// .populate('tags address');


		q.exec(function (err, results) {
			locals.data.companies = results;
			next(err);
		});
	});

	// Render the view
	view.render('companies');
};
