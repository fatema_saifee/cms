var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'blog';
	locals.filters = {
		contact: req.params.contact,
	};
	locals.data = {
		company: [],
	};

	// Load the current contact
	view.on('init', function (next) {

		var q = keystone.list('Com').model.findOne().populate('tags address');

		q.exec(function (err, result) {
			locals.data.contact = result;
			next(err);
		});

	});

	// Load other company
	view.on('init', function (next) {

		var q = keystone.list('Com').model.find();

		q.exec(function (err, results) {
			locals.data.company = results;
			console.log(locals.data.company);
			next(err);
		});

	});

	// Render the view
	view.render('company');
};
