var keystone = require('keystone');
var async = require('async');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Init locals
	locals.section = 'blog';
	
	locals.data = {
		contacts: [],
	};

	
	// Load the contacts
	view.on('init', function (next) {

		var q = keystone.list('Contact').paginate({
			page: req.query.page || 1,
			perPage: 10,
			maxPages: 10,
			// filters: {
			// 	state: 'published',
			// },
		})
			// .sort('-publishedDate')
			.populate('tags address');


		q.exec(function (err, results) {
			locals.data.contacts = results;
			next(err);
		});
	});

	// Render the view
	view.render('contacts_list');
};
