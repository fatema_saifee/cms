var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'blog';
	locals.filters = {
		contact: req.params.contact,
	};
	locals.data = {
		contacts: [],
	};

	// Load the current contact
	view.on('init', function (next) {

		var q = keystone.list('Contact').model.findOne().where('_id',req.params.contact).populate('tags address');
		console.log(q);
		q.exec(function (err, result) {
			locals.data.contact = result;
			next(err);
		});

	});

	// Load other contacts
	view.on('init', function (next) {

		var q = keystone.list('Contact').model.find();

		q.exec(function (err, results) {
			locals.data.contacts = results;
			next(err);
		});

	});

	// Render the view
	view.render('profile');
};
